package com.forrestjohnston.selenium.clientProjectName;

//import static junit.framework.Assert.assertEquals;
//import static junit.framework.Assert.assertTrue;
//import static junit.framework.Assert.assertFalse;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
// import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
//
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class StansburyTest {
	private WebDriver driver;
	private String pageTitle = "Stansberry & Associates Investment Research";
	private String baseUrl  = System.getProperty("webdriver.base.url");
	private String username = System.getProperty("stansbury.username");
	private String password = System.getProperty("stansbury.password");

	@BeforeSuite
	/* Print the prompt to the console and return the input line.
	 * We use BufferedReader instead of System.console so that it will
	 * work inside the IDE as well.
	 */
	public void checkProperties() {
		if (baseUrl == null  || baseUrl.length() < 1)
			baseUrl =  getConsoleInput("Enter base URL: ");
		if (username == null || username.length() < 1)
			username = getConsoleInput("Enter username: ");
		if (password == null || password.length() < 1)
			password = this.getPasswordDialog("Enter password: ");
	}
	
	
	@BeforeClass
	public void openBrowser() {
		Assert.assertTrue((baseUrl != null),  "baseUrl property not set ");
		Assert.assertTrue((username != null), "username property not set ");
		Assert.assertTrue((password != null), "password property not set ");
		driver = new FirefoxDriver();
		driver.get(baseUrl);
		
	}

	@AfterClass
	public void saveScreenshotAndCloseBrowser() throws IOException {
		ScreenshotHelper screenshotHelper = new ScreenshotHelper();
		screenshotHelper.saveScreenshot("screenshot.png");
		driver.quit();
	}

	@Test
	public void defaultPageLogin() throws IOException {
		Assert.assertTrue((driver.getTitle()).startsWith(pageTitle),
				"The page title should start with: " + pageTitle );
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement element = driver.findElement(By.name("username"));
		element.click();
		// password is hidden but displays when username clicked...
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("password")));
		element.sendKeys(username);
		element = driver.findElement(By.name("password"));
		element.click();
		element.sendKeys(password);
		element.submit();
		waitForPageLoaded(driver);
	}
	
	@Test(dependsOnMethods = "defaultPageLogin")
	public void getSubscriptions() throws IOException {
		// There are no unique id or name in the page to check
		// Navigate to table with image header "Your Subscriptions"
		WebElement element = findTableTag_ByContainsImgSrc(driver, "your_subs_title");
		Assert.assertNotNull(element, "Cannot find \"Your Subscriptions\" image");
		// Iterate through the table and get
		System.out.println(element.getAttribute("src"));
		element.click();
		waitForPageLoaded(driver);
	}

	// ===============================================================================================
	// ================================= Selenium-specific Utilities =================================
	// ===============================================================================================
	/**
	 * Return first Table element that has a cell with img.src matching searchStr.
	 * Assumes we are looking for the table that is the parent of the row/cell that
	 * contains the matching img.src
	 * 
	 * @param driver
	 * @param searchStr
	 * @return Table element that has a cell with img.src matching searchStr, or null if not found
	 */
	public WebElement findTableTag_ByContainsImgSrc(WebDriver driver, String searchStr) {
		WebElement element, rval = null;
		element = findImgTag_ByContainsSrc(driver, searchStr);
		while (element != null) {
			System.out.println("element.tag="+element.getTagName());
			if (element.getTagName().compareToIgnoreCase("table") == 0) {
				rval = element;
				break;
			}
			else {
				element = element.findElement(By.xpath(".."));
			}
		} 
		return rval;
	}
	
	public WebElement findImgTag_ByContainsSrc(WebDriver driver, String searchStr) {
		WebElement rval = null;
		List<WebElement> imageElements = driver.findElements(By.tagName("img"));
		for (WebElement imageElement : imageElements) {
		    if (imageElement.getAttribute("src").contains(searchStr)) {
		    	rval = imageElement;
		    	break;
		    }
		}
		return rval;
	}

	public void waitForPageLoaded(WebDriver driver) {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 30);
		try {
			wait.until(expectation);
		} catch (Throwable e) {
			Assert.assertFalse(true, 
					"Timeout waiting for Page Load Request to complete: " +
					e.getMessage());
		}
	}

	private class ScreenshotHelper {

		public void saveScreenshot(String screenshotFileName)
				throws IOException {
			File screenshot = ((TakesScreenshot) driver)
					.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File(screenshotFileName));
		}
	}

	// ===============================================================================================
	// =================================== Non-Selenium Utilities ====================================
	// ===============================================================================================
	
	/* Print the prompt to the console and return the input line.
	 * We use BufferedReader instead of System.console so that it will
	 * work inside the IDE as well.
	 */
	public String getConsoleInput(String prompt) {
		String rval = null;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	        System.out.print(prompt);
	        rval = br.readLine();
		}
		catch(IOException e) {
			System.err.println("caught IOException: " + e.getMessage());
		}
		return rval;
	}
	
	public String getPasswordDialog(String prompt) {
		String rval = null;
		JPasswordField pf = new JPasswordField();
		int okCxl = JOptionPane.showConfirmDialog(null, pf, prompt, JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
	
		if (okCxl == JOptionPane.OK_OPTION) {
		  rval = new String(pf.getPassword());
		  System.err.println("You entered: " + rval);
		}
		return rval;
	}
}
